#include <stdio.h>
extern void initialise_monitor_handles(void);
int main(void) {

  // Currently Cortex Debugger in VS Code doesn't support semihosting printing.
  // Running openOcd directly from console works fine. 
  // uncomment for semihosting
  // initialise_monitor_handles();
  int counter = 0U;
  while (1) {

    for (volatile int del = 0U; del < 100000; ++del)
      ;
   //   printf("SEMIHOSTING XD %d\n", counter++);
  }
  return 0;
}