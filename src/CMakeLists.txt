cmake_minimum_required(VERSION 3.18 FATAL_ERROR)

set(NAME STM_SEMIHOSTING)
set(PROJECT_NAME ${NAME}.elf)

set(BSP ${CMAKE_CURRENT_SOURCE_DIR}/BSP)
enable_language(C CXX ASM)

add_executable(${PROJECT_NAME}
    main.c
    ${BSP}/system_stm32f4xx.c
    ${BSP}/startup_stm32f429xx.s
)

target_compile_definitions(${PROJECT_NAME} PRIVATE
        # -DUSE_HAL_DRIVER
        -DSTM32F429xx
        )

target_compile_options(${PROJECT_NAME} PRIVATE
# MCU 
    -mcpu=cortex-m4
    -mthumb
    -mfpu=fpv4-sp-d16
    -mfloat-abi=hard

# Optimization
    #  -O3
    -fdata-sections
    -ffunction-sections

    -g
# Warnigs
    -Wall
    
)

target_include_directories(${PROJECT_NAME} PRIVATE
    ${BSP}/
)


target_link_options(${PROJECT_NAME} PRIVATE
#  Memory Map
    -T${BSP}/STM32F429ZI.ld
    -g
    # MCU 
    -mcpu=cortex-m4
    -mthumb
    -mfpu=fpv4-sp-d16
    -mfloat-abi=hard

    # Defautl specs
    # --specs=nosys.specs
    # Semihosting specs
    -specs=rdimon.specs
    -lrdimon

    -Wl,-Map=${PROJECT_NAME}.map,--cref
    -Wl,--gc-sections
    -Wl,--print-memory-usage
)

# Print executable size
add_custom_command(TARGET ${PROJECT_NAME}
        POST_BUILD
        COMMAND arm-none-eabi-size ${PROJECT_NAME}
)

# Create hex file
add_custom_command(TARGET ${PROJECT_NAME}
    POST_BUILD
    COMMAND arm-none-eabi-objcopy -O ihex ${PROJECT_NAME} ${PROJECT_NAME}.hex
    COMMAND arm-none-eabi-objcopy -O binary ${PROJECT_NAME} ${PROJECT_NAME}.bin
)