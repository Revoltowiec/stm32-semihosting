# stm32-semihosting

Run semihosting on STM32 MCU base on OpenOcd and GDB.
Run on STM32F429ZI from discavery board.

# Tools

- ARM GCC
https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads

- Ninja

- OpenOCD
https://gnutoolchains.com/arm-eabi/openocd/

# Build
cmake -Bbuild -S. -GNinja -DCMAKE_TOOLCHAIN_FILE="./cmake/toolchain.cmake"
cd build
ninja

# Debug
## run OpenOcd

openocd -f interface/stlink.cfg -f target/stm32f4x.cfg

## run gdb (under build directory)

arm-none-eabi-gdb -q -iex 'set pagination off' -x="..\debug_config\gdb.config" .\src\STM_SEMIHOSTING.elf


# Semihosting printf

Semihosting printf must ends with '\n'.